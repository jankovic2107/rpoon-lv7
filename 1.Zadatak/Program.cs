﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 12.4, 54.12, 12, 11, 99, 91.34, 24.65, 40, 21, 13.99 };
            NumberSequence numberSequence = new NumberSequence(array);
            SortStrategy bubbleSort = new BubbleSort();
            SortStrategy seqSort = new SequentialSort();
            SortStrategy combSort = new CombSort();

            numberSequence.SetSortStrategy(seqSort);
            numberSequence.Sort();
            Console.Write(numberSequence.ToString());
            Console.WriteLine();

            numberSequence.SetSortStrategy(bubbleSort);
            numberSequence.Sort();
            Console.Write(numberSequence.ToString());
            Console.WriteLine();

            numberSequence.SetSortStrategy(combSort);
            numberSequence.Sort();
            Console.Write(numberSequence.ToString());
            


        }
    }
}
