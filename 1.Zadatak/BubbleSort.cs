﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.Zadatak
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            int arraysize = array.Length;
            for (int i = 0; i < arraysize - 1; i++)

                for (int j = 0; j < arraysize - i - 1; j++)
                {
                    if (array[j] > array[j + 1])
                        Swap(ref array[j], ref array[j + 1]);

                }
        }
    }
}
