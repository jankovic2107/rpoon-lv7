﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd = new DVD("Amazing", DVDType.MOVIE, 19.99);
            VHS vhs = new VHS("Ancient", 9.99);
            Book book = new Book("Eragon", 9.99);

            BuyVisitor visitor = new BuyVisitor();
            Console.WriteLine(dvd.Accept(visitor));
            Console.WriteLine(vhs.Accept(visitor));
            Console.WriteLine(book.Accept(visitor));
        }
    }
}
