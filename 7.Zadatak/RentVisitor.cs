﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class RentVisitor : IVisitor
    {
        public double Visit(DVD DVDItem)
        {
            return DVDItem.Price;
        }

        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * 0.1;
        }

        public double Visit(Book bookItem)
        {
            return bookItem.Price * 0.1;
        }
    }
}
