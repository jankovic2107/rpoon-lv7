﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd1 = new DVD("Amazing", DVDType.MOVIE, 19.99);
            DVD dvd2 = new DVD("Educative", DVDType.SOFTWARE, 20);
            VHS vhs = new VHS("Ancient", 9.99);
            Book book = new Book("Eragon", 9.99);
            BuyVisitor buyVisitor = new BuyVisitor();
            RentVisitor rentItem = new RentVisitor();

            Cart cart = new Cart(buyVisitor);
            cart.AddItem(dvd1);
            cart.AddItem(dvd2);
            cart.AddItem(vhs);
            cart.AddItem(book);

            Console.WriteLine(cart.Accept());

            cart.SetVisitor(rentItem);
            Console.WriteLine(cart.Accept());
        }
    }
}
