﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class Cart
    {
        private List<IItem> items;
        IVisitor visitor;
        public Cart(IVisitor visitor)
        {
            this.visitor = visitor;
            this.items = new List<IItem>();
        }
        public void SetVisitor(IVisitor visitor)
        {
            this.visitor = visitor;
        }
        public void AddItem(IItem item)
        {
            this.items.Add(item);
        }
        public void RemoveItem(IItem item)
        {
            this.items.Remove(item);
        }
        public double Accept()
        {
            double temporaryValue = 0;
            foreach (IItem item in items)
            {
                if (item.Accept(visitor) != double.NaN)
                {
                    temporaryValue += item.Accept(visitor);
                }
            }
            return temporaryValue;
        }
    }
}
