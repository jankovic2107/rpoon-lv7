﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider systemDataProvider = new SystemDataProvider();
            FileLogger logger1 = new FileLogger(@"C:\Users\Ivan Jankovic\Desktop\RPOON-Lv7.txt");
            ConsoleLogger logger2 = new ConsoleLogger();
            
            systemDataProvider.Attach(logger1);
            systemDataProvider.Attach(logger2);

            while (true)
            {
                systemDataProvider.GetCPULoad();
                systemDataProvider.GetAvailableRAM();

                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
