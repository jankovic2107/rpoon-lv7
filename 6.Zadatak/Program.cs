﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd1 = new DVD("Amazing", DVDType.MOVIE, 19.99);
            DVD dvd2 = new DVD("Educative", DVDType.SOFTWARE, 20);
            VHS vhs = new VHS("Ancient", 9.99);
            Book book = new Book("Eragon", 9.99);

            RentVisitor rent = new RentVisitor();
            Console.WriteLine(dvd1.Accept(rent));
            Console.WriteLine(dvd2.Accept(rent));
            Console.WriteLine(vhs.Accept(rent));
            Console.WriteLine(book.Accept(rent));
        }
    }
}
