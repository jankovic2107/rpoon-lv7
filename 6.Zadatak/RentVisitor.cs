﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class RentVisitor : IVisitor
    {
        public double Visit(DVD DVDItem)
        {
            if(DVDType.SOFTWARE == DVDItem.Type)
            {
                return double.NaN;
            }
            return DVDItem.Price * 0.1;
        }

        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * 0.1;
        }

        public double Visit(Book bookItem)
        {
            return bookItem.Price * 0.1;
        }
    }
}
