﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 12.4, 54.12, 12, 11, 99, 91.34, 24.65, 40, 21, 13.99 };
            NumberSequence numberSequence = new NumberSequence(array);
            SortStrategy bubbleSort = new BubbleSort();
            SearchStrategy linearSearch = new LinearSearch(1);

            numberSequence.SetSortStrategy(bubbleSort);
            numberSequence.Sort();
            Console.Write(numberSequence.ToString());
            Console.WriteLine();

            numberSequence.SetSearchStrategy(linearSearch);
            Console.WriteLine(numberSequence.Search());
            

        }
    }
}
