﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    abstract class SearchStrategy
    {
        public abstract bool Search(double[] araay);
    }
}
