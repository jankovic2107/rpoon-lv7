﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class LinearSearch : SearchStrategy
    {
        private double Number;
        public LinearSearch(double Number)
        {
            this.Number = Number;
        }
        public override bool Search(double[] array)
        {
            int arraySize = array.Length;
            double found = 0;
            for (int i = 0; i < arraySize; i++)
            {
                if (array[i] == Number)
                {
                    found = 1;
                }
            }
            if (found == 1)
            {
                return true;
            }
            else
                return false;
        }
    }
}



